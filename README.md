# Task Manager Application

### Project Description

The following project is meant for managing tasks by breaking projects down into specific goals.

### Software Requirements:
```
1. Java 1.8
2. Maven 3.8.0
3. Spring MVC 5.0.8.RELEASE
4. Tomcat 7 2.2
5. Spring Security 5.0.0.RELEASE
```
### Technological Stack:
```
1. Spring context 2.0.7.RELEASE
2. Spring ORM 2.0.7.RELEASE
3. Spring data JPA 2.0.7.RELEASE
4. Hibernate 5.3.0.Final
5. Javax servlet 3.1.0
6. Javax servlet jsp-api 2.3.1
7. Jstl 1.2
```
### Developer Contact Information
```
Anastasia Kolevatykh
anastasia.kolevatykh@gmail.com
```
### Command for Application Building:
```
mvn clean install
```
### Command for Application Running:
```
tomcat:runwar
```

#### Admin page example
![Image admin](src/main/resources/img/admin_example.png)

#### User page example
![Image user](src/main/resources/img/user_example.png)