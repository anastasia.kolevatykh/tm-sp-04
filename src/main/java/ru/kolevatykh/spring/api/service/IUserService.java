package ru.kolevatykh.spring.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.spring.dto.UserDTO;
import ru.kolevatykh.spring.exception.DuplicateUserException;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.model.User;

import java.util.List;

public interface IUserService {
    @NotNull User getUserEntity(@Nullable UserDTO userDTO) throws Exception;

    @NotNull UserDTO getUserDTO(@Nullable User user) throws UserNotFoundException;

    @NotNull List<UserDTO> getListUserDTO(@Nullable List<User> users) throws UserNotFoundException;

    @Nullable User registerUser(User user) throws DuplicateUserException, UserNotFoundException;

    @NotNull List<User> findAll() throws Exception;

    @NotNull List<User> findAllBySearch(@Nullable String search) throws Exception;

    @Nullable User findOneById(@Nullable String id) throws Exception, UserNotFoundException;

    @Nullable User findOneByLogin(@Nullable String login) throws Exception, UserNotFoundException;

    void persist(@Nullable User user) throws UserNotFoundException;

    void merge(@Nullable User user) throws UserNotFoundException;

    void remove(@Nullable String id) throws Exception, UserNotFoundException;

    void removeAll();
}
