package ru.kolevatykh.spring;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kolevatykh.spring.api.client.AuthResourceClient;
import ru.kolevatykh.spring.api.controller.IRestLoginController;
import ru.kolevatykh.spring.service.PropertyService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = PropertyService.class)
@TestPropertySource
public class RestAuthControllerIT {

    @Autowired
    private PropertyService property;

    @Test
    public void testAuth() {
        IRestLoginController authResourceClient
                = AuthResourceClient.getInstance(property.getServer());
        String authStatus = authResourceClient.login("admin1", "admin1");
        System.out.println(authStatus);
        Assert.assertTrue(authStatus.contains("200"));
        authResourceClient.logout();
    }
}
