package ru.kolevatykh.spring;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;
import ru.kolevatykh.spring.dto.UserDTO;
import ru.kolevatykh.spring.dto.UserDTO;
import ru.kolevatykh.spring.enumerate.RoleType;
import ru.kolevatykh.spring.service.PropertyService;
import ru.kolevatykh.spring.util.PasswordHashUtil;

import java.util.Collections;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = PropertyService.class)
@TestPropertySource
public class RestUserControllerMvcIT {

    @Autowired
    private PropertyService property;

    @Nullable
    private UserDTO user;

    @NotNull
    private final RestTemplate restTemplate = new RestTemplate();

    @NotNull
    private final HttpHeaders headers = new HttpHeaders();

    @Before
    public void setUp() throws Exception {
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        @NotNull final String login = "testUser";
        @NotNull final String pass = "testUser";
        user = new UserDTO(login, pass, RoleType.valueOf("ADMIN"));
        @NotNull final HttpEntity<UserDTO> entityUser = new HttpEntity<>(user, headers);
        @NotNull String createUrl = property.getServer() + property.getUserMapping() + property.getRequestCreate();
        restTemplate.postForEntity(createUrl, entityUser, UserDTO.class);
    }

    @After
    public void tearDown() {
        @NotNull String deleteUrl = property.getServer() + property.getUserMapping() + property.getRequestDelete();
        restTemplate.delete(deleteUrl + user.getId());
    }

    @Test
    public void testFindAll() {
        @NotNull final String findAllUrl = property.getServer() + property.getUserMapping() + property.getRequestList();
        @NotNull final ResponseEntity<UserDTO[]> response = restTemplate.getForEntity(findAllUrl, UserDTO[].class);
        @Nullable final UserDTO[] users = response.getBody();
        Assert.assertNotNull(users);
        Assert.assertEquals(2, users.length);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testCreate() throws Exception {
        @NotNull final String login = "create";
        @NotNull final String pass = "create";
        @NotNull UserDTO temp = new UserDTO();
        temp.setLogin(login);
        temp.setPasswordHash(pass);
        temp.setRoleType(RoleType.valueOf("USER"));
        @NotNull final HttpEntity<UserDTO> entityProject = new HttpEntity<>(temp, headers);
        @NotNull final String createUrl = property.getServer() + property.getUserMapping() + property.getRequestCreate();
        @NotNull final ResponseEntity<UserDTO> response = restTemplate.postForEntity(createUrl, entityProject, UserDTO.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        @NotNull String deleteUrl = property.getServer() + property.getUserMapping() + property.getRequestDelete();
        restTemplate.delete(deleteUrl + temp.getId());
    }

    @Test
    public void testUpdate() throws Exception {
        @NotNull final String login = "update";
        @NotNull final String pass = "update";
        user.setLogin(login);
        user.setPasswordHash(pass);
        @NotNull final HttpEntity<UserDTO> entityProject = new HttpEntity<>(user, headers);
        @NotNull final String updateUrl = property.getServer() + property.getUserMapping() + property.getRequestUpdate();
        @NotNull final String url = updateUrl + user.getId();
        @NotNull final ResponseEntity<UserDTO> response = restTemplate.postForEntity(url, entityProject, UserDTO.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDelete() throws Exception {
        @NotNull final String login = "delete";
        @NotNull final String pass = "delete";
        @NotNull UserDTO temp = new UserDTO();
        temp.setLogin(login);
        temp.setPasswordHash(pass);
        temp.setRoleType(RoleType.valueOf("USER"));
        @NotNull final HttpEntity<UserDTO> entityProject = new HttpEntity<>(temp, headers);
        @NotNull final String createUrl = property.getServer() + property.getUserMapping() + property.getRequestCreate();
        @NotNull final ResponseEntity<UserDTO> response = restTemplate.postForEntity(createUrl, entityProject, UserDTO.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        @NotNull String deleteUrl = property.getServer() + property.getUserMapping() + property.getRequestDelete();
        @NotNull String url = deleteUrl + temp.getId();
        restTemplate.delete(url);
    }

    @Test
    public void testView() throws Exception {
        @NotNull final String viewUrl = property.getServer() + property.getUserMapping() + property.getRequestView();
        @NotNull String url = viewUrl + user.getId();
        @NotNull final ResponseEntity<UserDTO> response = restTemplate.getForEntity(url, UserDTO.class);
        @Nullable final UserDTO temp = response.getBody();
        Assert.assertNotNull(temp);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}